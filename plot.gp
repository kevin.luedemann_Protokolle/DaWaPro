reset

set terminal epslatex color

set ylabel "ln(p) [ln(Pascal)]"
set xlabel "1/T [1/Kelvin]"
f(x)=m*x+b
A=3.908*10**(-3)
B=-5.775*10**(-7)
R=1000
set output 'erw.tex'

set fit logfile "erw.log"
fit f(x) "dat.txt" u (1/(((-A/(2*B))-sqrt((A**2/(4*B**2))-((R-$2)/(R*B))))+273)):(log($1*100000+101700)):(0.5/$1) via m,b
p f(x) t "Regression", "dat.txt" u (1/(((-A/(2*B))-sqrt((A**2/(4*B**2))-(R-$2)/(R*B)))+273)):(log($1*100000+101700)):(1/$1) w e t "Messwerte"
set output

set output 'ab.tex'

set fit logfile "ab.log"
fit f(x) "dat.txt" u (1/(((-A/(2*B))-sqrt((A**2/(4*B**2))-((R-$3)/(R*B))))+273)):(log($1*100000+101700)):(0.1/$1) via m,b
p f(x) t "Regression", "dat.txt" u (1/(((-A/(2*B))-sqrt((A**2/(4*B**2))-(R-$3)/(R*B)))+273)):(log($1*100000+101700)):(1/$1) w e t "Messwerte"
set output
